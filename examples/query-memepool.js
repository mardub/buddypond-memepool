let m = require('../Mnemosyne');

// directish match
for (let i = 0; i < 6; i++) {
  let card = m.query('cloud');
  console.log(card.title);
}

// randomish
for (let i = 0; i < 6; i++) {
  let card = m.query('win lose advice wisdom fren');
  console.log(card.title);
}

// single word match
for (let i = 0; i < 6; i++) {
  let card = m.query('cool');
  console.log(card.title);
}

// directish match
for (let i = 0; i < 6; i++) {
  let card = m.query('albert einstein');
  console.log(card.title);
}


