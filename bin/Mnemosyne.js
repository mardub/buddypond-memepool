var argv = require('minimist')(process.argv.slice(2));
let searchString = argv._.join(' ');

const Mnemosyne = require('../Mnemosyne');

console.log(Mnemosyne.query(searchString));